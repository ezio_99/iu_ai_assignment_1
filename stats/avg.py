from collections import defaultdict
import re

FILES = ["map1.txt", "map2.txt", "map3.txt", "map4.txt", "map5.txt"]

stats = {}

current_algorithm = None
commas_between_digits_regex = re.compile(r"(?P<g1>\d),(?P<g2>\d)")
stats_regex = re.compile(r"^% (?P<inferences>[\d.]+) inferences, (?P<cpu>[\d.]+)"
                         r" CPU in (?P<time>[\d.]+) seconds \((?P<cpu_percentage>[\d.]+)% CPU, "
                         r"(?P<lips>[\d.]+) Lips\)$")

for file in FILES:
    map_ = file.split(".")[0]
    stats[map_] = defaultdict(lambda: defaultdict(int))
    with open(file, "r", encoding="utf-8") as f:
        for line in f:
            if not (stripped := line.strip()):
                continue
            if stripped.startswith("Algorithm"):
                current_algorithm = stripped.split(":")[1].strip()
            else:
                stats_str = commas_between_digits_regex.sub(r"\g<g1>\g<g2>", line)
                match_dict = stats_regex.match(stats_str).groupdict()
                stats[map_][current_algorithm]["count"] += 1
                stats[map_][current_algorithm]["total_inferences"] += float(match_dict["inferences"])
                stats[map_][current_algorithm]["total_cpu"] += float(match_dict["cpu"])
                stats[map_][current_algorithm]["total_time"] += float(match_dict["time"])
                stats[map_][current_algorithm]["total_cpu_percentage"] += float(match_dict["cpu_percentage"])
                stats[map_][current_algorithm]["total_lips"] += float(match_dict["lips"])

for stats_ in stats.values():
    for stats__ in stats_.values():
        stats__["avg_inferences"] = stats__["total_inferences"] / stats__["count"]
        stats__["avg_cpu"] = stats__["total_cpu"] / stats__["count"]
        stats__["avg_time"] = stats__["total_time"] / stats__["count"]
        stats__["avg_cpu_percentage"] = stats__["total_cpu_percentage"] / stats__["count"]
        stats__["avg_lips"] = stats__["total_lips"] / stats__["count"]

for map_, stats_ in stats.items():
    print(f"{map_}:")
    for algorithm, stats__ in stats_.items():
        print(f"    {algorithm}:")
        for k, v in stats__.items():
            print(f"        {k}: {v}")
        print()
    print()

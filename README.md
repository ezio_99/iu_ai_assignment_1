# Info about project
This repository contains solution for Assignment 1 for the course Introduction to AI (Spring 2021).

# Info about realization
Maps can be imported using include syntax(`:- ['map.pl'].`). If map is imported, new map is not generated. Map defining syntax can be find in one of example map (see `maps` directory).

In `main.pl` can be specified fact `is_user_input(Value).`. Value can be 0 or 1, which tells the system use user input or no. If `read` predicate is called manually (see below), this value will be ignored.

Also world params can be defined in `main.pl`.

# Top level commands
`read` - Read world params for map generation from user.

`new_map` - Generate new map. Also have shortcut `new`.

----

Test functions:
* `test_backtracking1` - Test function for backtracking with covid perception 1. Also have shortcut `b1`.
* `test_backtracking2` - Test function for backtracking with covid perception 2. Also have shortcut `b2`.
* `test_a_star1` - Test function for A* with covid perception 1. Also have shortcut `a1`.
* `test_a_star2` - Test function for A* with covid perception 2. Also have shortcut `a2`.

# Use cases
`new, init_world, print_board.` - Generate new map and print it.

`new, a1.` - Generate new map, find path using A* 1.

`read, new, a1.` - Read world params, generate new map, find path using A* 1.

`new, a1, b1.` - Generate new world, find path using A* 1 and Bactracking 1.

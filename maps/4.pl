%% ---------------------
%% | . . . . . . . H . |
%% | . . . ! ! ! * . . |
%% | . . . ! C * . . . |
%% | . . . ! * ! ! ! . |
%% | . . . * . ! C ! . |
%% | . . . . M ! ! ! . |
%% | . . . * . . D . . |
%% | . . * . . . . . . |
%% | A * . . . . . . . |
%% ---------------------

world_dimension([9, 9]).
start_position([0, 0]).

home_position(7, 8).
mask_position(4, 3).
doctor_position(6, 2).
covid_position(4, 6).
covid_position(6, 4).

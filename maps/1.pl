%% ---------------------
%% | . . * * H . . . D |
%% | . * ! ! ! ! ! ! M |
%% | . * ! C ! ! C ! . |
%% | . * ! ! ! ! ! ! . |
%% | . . * . . . . . . |
%% | . . . * . . . . . |
%% | . . * . . . . . . |
%% | . * . . . . . . . |
%% | A . . . . . . . . |
%% ---------------------

world_dimension([9, 9]).
start_position([0, 0]).

home_position(4, 8).
mask_position(8, 7).
doctor_position(8, 8).
covid_position(3, 6).
covid_position(6, 6).

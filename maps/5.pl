%% ---------------------------------
%% | . . . ! C ! . D . . . . . . . |
%% | . . . ! ! ! . . M . . . . . . |
%% | D . . . . . . . . . . H . . . |
%% | . . . ! ! ! . . . . * . . . . |
%% | ! ! ! ! C ! . . . * . . . . . |
%% | ! C ! ! ! ! . . * . . . . . . |
%% | ! ! ! . . ! ! ! . M . ! ! ! . |
%% | . . . . . ! C ! * . D ! C ! . |
%% | . . . . . ! ! ! * . . ! ! ! . |
%% | . . . . . . * * . . . . . . M |
%% | . . . . . * ! ! ! . . . . . . |
%% | . . . . * . ! C ! . . . . . . |
%% | . . . * . . ! ! ! . . . . . . |
%% | . . * ! ! ! . . . . . . . . . |
%% | A * . ! C ! . . . . . . . . . |
%% ---------------------------------

world_dimension([15, 15]).
start_position([0, 0]).

home_position(11, 12).

mask_position(8, 13).
mask_position(9, 8).
mask_position(14, 5).

doctor_position(0, 12).
doctor_position(7, 14).
doctor_position(10, 7).

covid_position(1, 9).
covid_position(4, 0).
covid_position(4, 10).
covid_position(4, 14).
covid_position(6, 7).
covid_position(7, 3).
covid_position(12, 7).

%% ---------------------
%% | D . . . . . . . . |
%% | . . . . . . . . . |
%% | . . . . . . . . . |
%% | . . . . . . . . . |
%% | . . . . * * * M . |
%% | . * * * ! ! ! * . |
%% | * ! ! ! ! C * . . |
%% | * ! C ! ! * ! . . |
%% | A ! ! ! H . . . . |
%% ---------------------

world_dimension([9, 9]).
start_position([0, 0]).

home_position(4, 0).
mask_position(7, 4).
doctor_position(0, 8).
covid_position(2, 1).
covid_position(5, 2).

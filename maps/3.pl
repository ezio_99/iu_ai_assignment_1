%% ---------------------
%% | H ! C ! . . . . . |
%% | ! * ! ! . . . . . |
%% | C ! * . . . . . . |
%% | ! ! . * . . . . . |
%% | . . . . D . . . . |
%% | . . . * . . . . . |
%% | . . * . . . . . . |
%% | . * . . . . . M . |
%% | A . . . . . . . . |
%% ---------------------

world_dimension([9, 9]).
start_position([0, 0]).

home_position(0, 8).
mask_position(7, 1).
doctor_position(4, 4).
covid_position(0, 6).
covid_position(2, 8).

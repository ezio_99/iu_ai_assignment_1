% ======================================================================================
% A* SEARCH
% ======================================================================================

:- dynamic([
    open/1,      % stores list for open cells,   [[Priority, Cell, From, IsCovidDangerous], ...]
    closed/1     % stores list for closed cells, [[Priority, Cell, From, IsCovidDangerous], ...]
]).

% [[Priority, Cell, From, IsCovidDangerous], ...]
% Priority         - distance from start
% Cell             - cell itself
% From             - from which cell we can reach Cell
% IsCovidDangerous - with or without protection we move to this cell
% IsCovidDangerous - 1 - Yes (without protection)
% IsCovidDangerous - 0 - No (with protection)

% ======================================================================================

% set new value to open
set_open(Open):-
    retractall(open(_)),
    sort(Open, Open1),
    assert(open(Open1)).

% remove cell from open list
remove_from_open(Cell, IsCovidDangerous):-
    open(Open),
    delete(Open, [_, Cell, _, IsCovidDangerous], Open1),
    set_open(Open1).

% ======================================================================================

% add given list of cells to open list

add_cells_list_to_open([], _, _, _).

add_cells_list_to_open([Cell], Priority, From, IsCovidDangerous):-
    add_cell_to_open(Priority, Cell, From, IsCovidDangerous).

add_cells_list_to_open([Cell | T], Priority, From, IsCovidDangerous):-
    add_cell_to_open(Priority, Cell, From, IsCovidDangerous),
    add_cells_list_to_open(T, Priority, From, IsCovidDangerous).

% ======================================================================================

% add given cell to open list

add_cell_to_open(_, Cell, _, IsCovidDangerous):-
    closed(Closed),
    member([_, Cell, _, IsCovidDangerous], Closed), !.

add_cell_to_open(Priority, Cell, From, IsCovidDangerous):-
    closed(Closed),
    \+ member([_, Cell, _, IsCovidDangerous], Closed),
    open(Open),
    member([OldPriority, Cell, _, IsCovidDangerous], Open),
    (OldPriority > Priority ->
        delete(Open, [OldPriority, Cell, _, IsCovidDangerous], Open_),
        append(Open_, [[Priority, Cell, From, IsCovidDangerous]], Open1),
        set_open(Open1)
        ;
        true
    ).

add_cell_to_open(Priority, Cell, From, IsCovidDangerous):-
    closed(Closed),
    \+ member([_, Cell, _, IsCovidDangerous], Closed),
    open(Open),
    \+ member([_, Cell, _, IsCovidDangerous], Open),
    append(Open, [[Priority, Cell, From, IsCovidDangerous]], Open1),
    set_open(Open1).

% ======================================================================================

% set new value to closed
set_closed(Closed):-
    retractall(closed(_)),
    assert(closed(Closed)).

% add new cell to closed
add_to_closed(_, Cell, _, IsCovidDangerous):-
    closed(Closed),
    member([_, Cell, _, IsCovidDangerous], Closed).

add_to_closed(Priority, Cell, From, IsCovidDangerous):-
    closed(Closed),
    \+ member([_, Cell, _, IsCovidDangerous], Closed),
    append(Closed, [[Priority, Cell, From, IsCovidDangerous]], Closed1),
    set_closed(Closed1).

% ======================================================================================

% get adjacents without covid
get_adjacents_without_covid(Cell, Perceptor, Adjacent):-
    adjacent(Cell, Adjacent),
    \+ call(Perceptor, Cell, Adjacent).

% get adjacents depending on IsCovidDangerous flag
get_adjacents(Cell, IsCovidDangerous, _, Adjacents):-
    IsCovidDangerous == 0,
    setof(A, adjacent(Cell, A), Adjacents).

get_adjacents(Cell, IsCovidDangerous, Perceptor, Adjacents):-
    IsCovidDangerous == 1,
    setof(A, get_adjacents_without_covid(Cell, Perceptor, A), Adjacents).

% ======================================================================================

% get shortest path from visited list
% [] denotes no path

get_path(To, Closed, Path):- \+ member([_, To, _, _], Closed), Path = [].

get_path(To, Closed, Path):-
    member([Priority, To, _, IsCovidDangerous], Closed),
    Priority1 is Priority + 1,
    (setof(P, get_path_from_closed([To], Closed, Priority1, IsCovidDangerous, P), Paths) ->
        true
        ;
        Paths = []
    ),
    get_shortest_path(Paths, Path1),
    reverse(Path1, Path).

% ======================================================================================

% traverse from destination to start for retrieving reverse path from start to destination

get_path_from_closed(CurrentPath, Closed, _, _, Path):-
    last(CurrentPath, Last),
    member([0, Last, From, 1], Closed),
    From == [],
    Path = CurrentPath, !.

get_path_from_closed(CurrentPath, Closed, Priority, IsCovidDangerous, Path):-
    IsCovidDangerous == 1,
    last(CurrentPath, Last),
    Priority1 is Priority - 1,
    member([Priority1, Last, From, IsCovidDangerous], Closed),
    From \== [],
    append(CurrentPath, [From], CurrentPath1),
    get_path_from_closed(CurrentPath1, Closed, Priority1, IsCovidDangerous, Path).

get_path_from_closed(CurrentPath, Closed, Priority, IsCovidDangerous, Path):-
    IsCovidDangerous == 0,
    last(CurrentPath, Last),
    Priority1 is Priority - 1,
    member([Priority1, Last, From, IsCovidDangerous], Closed),
    From \== [],
    append(CurrentPath, [From], CurrentPath1),
    get_path_from_closed(CurrentPath1, Closed, Priority1, IsCovidDangerous, Path).

get_path_from_closed(CurrentPath, Closed, Priority, IsCovidDangerous, Path):-
    IsCovidDangerous == 0,
    last(CurrentPath, Last),
    Priority1 is Priority - 1,
    \+ member([Priority1, Last, _, IsCovidDangerous], Closed),
    IsCovidDangerous1 = 1,
    member([Priority1, Last, From, IsCovidDangerous1], Closed),
    From \== [],
    append(CurrentPath, [From], CurrentPath1),
    get_path_from_closed(CurrentPath1, Closed, Priority1, IsCovidDangerous1, Path).

% ======================================================================================

% initialization before search
init_a_star(StartPosition):-
    set_open([[0, StartPosition, [], 1]]),
    set_closed([]).

% remove unnecessary facts after search
a_star_teardown:-
    retractall(open(_)),
    retractall(closed(_)).

% general a_star
a_star__(From, To, Perceptor, ShortestPath):-
    init_a_star(From),
    (a_star(To, Perceptor) ->
        closed(Closed),
        get_path(To, Closed, ShortestPath)
        ;
        ShortestPath = []
    ),
    a_star_teardown.

% a_star with variant1
a_star_1(From, To, ShortestPath):- a_star__(From, To, perceptor1, ShortestPath).

% a_star with variant2
a_star_2(From, To, ShortestPath):- a_star__(From, To, perceptor2, ShortestPath).

% ======================================================================================

% a_star(To, Perceptor)
% To        - Destination coordinates
% Perceptor - Covid perceptor

% 1. Case when we reach home
a_star(To, _):-
    open(Open),
    length(Open, L),
    L \== 0,

    nth1(1, Open, [Priority, Cell, From, IsCovidDangerous]),
    Cell == To,
    add_to_closed(Priority, Cell, From, IsCovidDangerous),
    set_open([]).

% 2. Base case, open is empty - algorithm terminates
a_star(_, _):-
    open(Open),
    length(Open, L),
    L == 0, !.

% 3. Case when we find doctor or mask
a_star(To, Perceptor):-
    open(Open),
    length(Open, L),
    L \== 0,

    nth1(1, Open, [Priority, Cell, From, IsCovidDangerous]),

    is_covid_is_not_dangerous(Cell),

    remove_from_open(Cell, IsCovidDangerous),
    IsCovidDangerous1 = 0,
    get_adjacents(Cell, IsCovidDangerous1, Perceptor, Adjacents),
    Priority1 is Priority + 1,
    add_cells_list_to_open(Adjacents, Priority1, Cell, IsCovidDangerous1),
    add_to_closed(Priority, Cell, From, IsCovidDangerous),

    a_star(To, Perceptor).

% 4. Case when we just move if minimum is not reached
a_star(To, Perceptor):-
    open(Open),
    length(Open, L),
    L \== 0,

    nth1(1, Open, [Priority, Cell, From, IsCovidDangerous]),
    remove_from_open(Cell, IsCovidDangerous),
    get_adjacents(Cell, IsCovidDangerous, Perceptor, Adjacents),
    Priority1 is Priority + 1,
    add_cells_list_to_open(Adjacents, Priority1, Cell, IsCovidDangerous),
    add_to_closed(Priority, Cell, From, IsCovidDangerous),

    a_star(To, Perceptor).

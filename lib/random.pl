% ======================================================================================
% MAP GENERATION
% ======================================================================================

:- dynamic([
    world_dimension/1,  % world_dimension([X, Y])
    start_position/1,   % start_position([X, Y])
    home_position/2,    % home_position(X, Y)
    mask_position/2,    % mask_position(X, Y)
    doctor_position/2,  % doctor_position(X, Y)
    covid_position/2    % covid_position(X, Y)
]).

% clear map
new_map:-
    retractall(world_dimension(_)),
    retractall(start_position(_)),
    retractall(home_position(_, _)),
    retractall(mask_position(_, _)),
    retractall(doctor_position(_, _)),
    retractall(covid_position(_, _)).

new:- new_map.

% generates new map
generate_map(X, Y, MaskCount, DoctorCount, CovidCount):-
    generate_world(X, Y),
    generate_agent([], [], [], OccupiedCells1, CovidFreeCells1, InfectedCells1),
    generate_home(OccupiedCells1, CovidFreeCells1, InfectedCells1, OccupiedCells2, CovidFreeCells2, InfectedCells2),
    generate_covid(CovidCount, OccupiedCells2, CovidFreeCells2, InfectedCells2,
                   OccupiedCells3, CovidFreeCells3, InfectedCells3),
    generate_mask(MaskCount, OccupiedCells3, CovidFreeCells3, InfectedCells3,
                  OccupiedCells4, CovidFreeCells4, InfectedCells4),
    generate_doctor(DoctorCount, OccupiedCells4, CovidFreeCells4, InfectedCells4, _, _, _).

% wrapper for random, result will be in [X, Y] instead of [X, Y)
random_(X, Y, R):-
    Y1 is Y + 1,
    random(X, Y1, R).

% check if there are enough space for generating covid
check_empty_space_for_covid(OccupiedCells, CovidFreeCells):-
    Item = 'Covid',
    length(OccupiedCells, LengthOccupiedCells),
    length(CovidFreeCells, LengthCovidFreeCells),
    world_dimension([X, Y]),
    CellCount is X * Y,
    L is LengthOccupiedCells + LengthCovidFreeCells,
    (L >= CellCount -> print_no_space_error_log(Item), false; true).

% check if there are enough space for generating item (item cannot be covid)
check_empty_space(OccupiedCells, InfectedCells, Item):-
    length(OccupiedCells, LengthOccupiedCells),
    length(InfectedCells, LengthInfectedCells),
    world_dimension([X, Y]),
    CellCount is X * Y,
    L is LengthOccupiedCells + LengthInfectedCells,
    (L >= CellCount -> print_no_space_error_log(Item), false; true).

print_no_space_error_log(Item):-
    format('~nError: there are no space for ~w~n', [Item]).

% generate world
generate_world(X, Y):-
    \+ world_dimension([_, _]),
    assert(world_dimension([X, Y])).

% generate agent
% if no space - fail with error
generate_agent(OccupiedCells, CovidFreeCells, InfectedCells, OccupiedCells1, CovidFreeCells1, InfectedCells1):-
    \+ start_position([_, _]),
    check_empty_space(OccupiedCells, InfectedCells, 'Agent'),
    AgentPosition = [0, 0],
    assert(start_position(AgentPosition)),

    append(OccupiedCells, [AgentPosition], OccupiedCells_),
    list_to_set(OccupiedCells_, OccupiedCells1),

    bagof(A, adjacent(AgentPosition, A), Adjacents),
    append(CovidFreeCells, Adjacents, CovidFreeCells_),
    list_to_set(CovidFreeCells_, CovidFreeCells1),

    InfectedCells1 = InfectedCells.

% generate home
% if no space - fail with error
generate_home(OccupiedCells, CovidFreeCells, InfectedCells, OccupiedCells1, CovidFreeCells1, InfectedCells1):-
    check_empty_space(OccupiedCells, InfectedCells, 'Home'),
    world_dimension([X, Y]),
    random(0, X, X_),
    random(0, Y, Y_),
    HomePosition = [X_, Y_],
    (
        \+ member(HomePosition, OccupiedCells),
        \+ member(HomePosition, InfectedCells) ->

        assert(home_position(X_, Y_)),
        append(OccupiedCells, [HomePosition], OccupiedCells_),
        list_to_set(OccupiedCells_, OccupiedCells1),

        bagof(A, adjacent(HomePosition, A), Adjacents),
        append(CovidFreeCells, Adjacents, CovidFreeCells_),
        list_to_set(CovidFreeCells_, CovidFreeCells1),

        InfectedCells1 = InfectedCells
        ;
        generate_home(OccupiedCells, CovidFreeCells, InfectedCells, OccupiedCells1, CovidFreeCells1, InfectedCells1), true
    ).

% generate mask
% if no space - fail with error
generate_mask(Count, OccupiedCells, CovidFreeCells, InfectedCells, OccupiedCells, CovidFreeCells, InfectedCells):- Count =< 0.

generate_mask(Count, OccupiedCells, CovidFreeCells, InfectedCells, OccupiedCells1, CovidFreeCells1, InfectedCells1):-
    Count > 0,

    check_empty_space(OccupiedCells, InfectedCells, 'Mask'),
    world_dimension([X, Y]),
    random(0, X, X_),
    random(0, Y, Y_),
    MaskPosition = [X_, Y_],
    (
        \+ member(MaskPosition, OccupiedCells),
        \+ member(MaskPosition, InfectedCells) ->

        assert(mask_position(X_, Y_)),
        append(OccupiedCells, [MaskPosition], OccupiedCells_),
        list_to_set(OccupiedCells_, OccupiedCells__),

        bagof(A, adjacent(MaskPosition, A), Adjacents),
        append(CovidFreeCells, Adjacents, CovidFreeCells_),
        list_to_set(CovidFreeCells_, CovidFreeCells__),

        Count1 is Count - 1,
        generate_mask(Count1, OccupiedCells__, CovidFreeCells__, InfectedCells, OccupiedCells1, CovidFreeCells1, InfectedCells1)
        ;
        generate_mask(Count, OccupiedCells, CovidFreeCells, InfectedCells, OccupiedCells1, CovidFreeCells1, InfectedCells1)
    ).

% generate doctor
% if no space - fail with error
generate_doctor(Count, OccupiedCells, CovidFreeCells, InfectedCells, OccupiedCells, CovidFreeCells, InfectedCells):- Count =< 0.

generate_doctor(Count, OccupiedCells, CovidFreeCells, InfectedCells, OccupiedCells1, CovidFreeCells1, InfectedCells1):-
    Count > 0,
    check_empty_space(OccupiedCells, InfectedCells, 'Doctor'),
    world_dimension([X, Y]),
    random(0, X, X_),
    random(0, Y, Y_),
    DoctorPosition = [X_, Y_],
    (
        \+ member(DoctorPosition, OccupiedCells),
        \+ member(DoctorPosition, InfectedCells) ->

        assert(doctor_position(X_, Y_)),
        append(OccupiedCells, [DoctorPosition], OccupiedCells_),
        list_to_set(OccupiedCells_, OccupiedCells__),

        bagof(A, adjacent(DoctorPosition, A), Adjacents),
        append(CovidFreeCells, Adjacents, CovidFreeCells_),
        list_to_set(CovidFreeCells_, CovidFreeCells__),

        Count1 is Count - 1,
        generate_doctor(Count1, OccupiedCells__, CovidFreeCells__, InfectedCells, OccupiedCells1, CovidFreeCells1, InfectedCells1)
        ;
        generate_doctor(Count, OccupiedCells, CovidFreeCells, InfectedCells, OccupiedCells1, CovidFreeCells1, InfectedCells1)
    ).

% generate covid
% if no space - fail with error
generate_covid(Count, OccupiedCells, CovidFreeCells, InfectedCells, OccupiedCells, CovidFreeCells, InfectedCells):- Count =< 0.

generate_covid(Count, OccupiedCells, CovidFreeCells, InfectedCells, OccupiedCells1, CovidFreeCells1, InfectedCells1):-
    Count > 0,

    check_empty_space_for_covid(OccupiedCells, CovidFreeCells),
    world_dimension([X, Y]),
    random(0, X, X_),
    random(0, Y, Y_),
    CovidPosition = [X_, Y_],
    (
        \+ member(CovidPosition, OccupiedCells),
        \+ member(CovidPosition, CovidFreeCells) ->

        assert(covid_position(X_, Y_)),
        append(OccupiedCells, [CovidPosition], OccupiedCells_),
        list_to_set(OccupiedCells_, OccupiedCells__),

        bagof(A, adjacent(CovidPosition, A), Adjacents),
        append(InfectedCells, Adjacents, InfectedCells_),
        list_to_set(InfectedCells_, InfectedCells__),

        Count1 is Count - 1,
        generate_covid(Count1, OccupiedCells__, CovidFreeCells, InfectedCells__, OccupiedCells1, CovidFreeCells1, InfectedCells1)
        ;
        generate_covid(Count, OccupiedCells, CovidFreeCells, InfectedCells, OccupiedCells1, CovidFreeCells1, InfectedCells1)
    ).

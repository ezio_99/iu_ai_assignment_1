% ======================================================================================
% BACKTRACKING
% ======================================================================================

:- dynamic([
    current_minimum/1    % possible minimum of steps
]).

% ======================================================================================

% return initial possible minimum
get_minimum(Min):-
    world_size([X, Y]),
    Min is X * Y + 1.

% set new value to possible minimum
set_current_minimum(Value):-
    retractall(current_minimum(_)),
    assert(current_minimum(Value)).

% check that distance from start plus distance to destination from current point is lower than possible minimum
check_current_minimum(From, To, Count):-
    distance(From, To, D),
    current_minimum(CurrentMinimum),
    D1 is D + Count,
    D1 < CurrentMinimum.

% ======================================================================================

% get adjacent with distance changing (Priority) depending on have agent protection or not
% Z = [Priority, X, Y]
get_next(From, To, IsCovidDangerous, LeadingPath, Perceptor, Z):-
    adjacent(From, AdjacentPosition),
    \+ member(AdjacentPosition, LeadingPath),
    IsCovidDangerous == 1, \+ call(Perceptor, From, AdjacentPosition),
    distance(From, To, Ch),
    distance(AdjacentPosition, To, Ah),
    P is Ch - Ah,
    Z = [P, AdjacentPosition].

get_next(From, To, IsCovidDangerous, LeadingPath, _, Z):-
    adjacent(From, AdjacentPosition),
    \+ member(AdjacentPosition, LeadingPath),
    IsCovidDangerous == 0,
    distance(From, To, Ch),
    distance(AdjacentPosition, To, Ah),
    P is Ch - Ah,
    Z = [P, AdjacentPosition].

% sort by Priority
% L = [[Priority, X, Y], ...]
sort_by_distance(L, Sorted):- sort(0, >=, L, Sorted).

% return sorted list of points sorted by Priority
get_next_points_with_prioritets(From, To, IsCovidDangerous, LeadingPath, Perceptor, Sorted):-
    bagof(Z, get_next(From, To, IsCovidDangerous, LeadingPath, Perceptor, Z), L),
    sort_by_distance(L, Sorted).

% ======================================================================================

% initialization before search
init_backtracking:-
    get_minimum(Min),
    set_current_minimum(Min).

% remove unnecessary facts after search
backtracking_teardown:-
    retractall(current_minimum(_)).

% general backtracking
backtracking__(From, To, Perceptor, ShortestPath):-
    init_backtracking,
    (setof(P, backtracking(From, To, [], 1, 1, -1, Perceptor, P), Paths) -> true; Paths = []),
    get_shortest_path(Paths, ShortestPath),
    backtracking_teardown.

% backtracking with variant2
backtracking_1(From, To, ShortestPath):- backtracking__(From, To, perceptor1, ShortestPath).

% backtracking with variant2
backtracking_2(From, To, ShortestPath):- backtracking__(From, To, perceptor2, ShortestPath).

% ======================================================================================

% backtracking(From, To, CurrentPath, IsCovidDangerous, Priority, Count, Perceptor, ResultPath)
% From             - Current coordinates
% To               - Destination coordinates
% CurrentPath      - Travelled path
% IsCovidDangerous - Have protection or not (0 - yes, 1 - no)
% Priority         - Priority of current coordinates
% Count            - Number of travelled cells
% Perceptor        - Covid perceptor
% ResultPath       - Resulting path ending in point To

% 1. Base case, when we reach destination
backtracking(From, To, LeadingPath, _, _, Count, _, ResultPath):-
    Count1 is Count + 1,
    current_minimum(CurrentMinimum),
    Count1 =< CurrentMinimum,

    From == To,
    append(LeadingPath, [From], ResultPath),
    set_current_minimum(Count1), !.

% 2. Case when we find doctor or mask
backtracking(From, To, LeadingPath, _, _, Count, Perceptor, ResultPath):-
    is_covid_is_not_dangerous(From),

    Count1 is Count + 1,
    check_current_minimum(From, To, Count1),

    IsCovidDangerous = 0,
    append(LeadingPath, [From], CurrentPath),

    get_next_points_with_prioritets(From, To, IsCovidDangerous, CurrentPath, Perceptor, Sorted),
    get(Sorted, [Priority1, From1]),
    backtracking(From1, To, CurrentPath, IsCovidDangerous, Priority1, Count1, Perceptor, ResultPath).

% 3. Case when we move without protection
backtracking(From, To, LeadingPath, IsCovidDangerous, _, Count, Perceptor, ResultPath):-
    IsCovidDangerous == 1,

    Count1 is Count + 1,
    check_current_minimum(From, To, Count1),

    append(LeadingPath, [From], CurrentPath),

    get_next_points_with_prioritets(From, To, IsCovidDangerous, CurrentPath, Perceptor, Sorted),
    get(Sorted, [Priority1, From1]),
    backtracking(From1, To, CurrentPath, IsCovidDangerous, Priority1, Count1, Perceptor, ResultPath).

% 4. Case when we move with protection
backtracking(From, To, LeadingPath, IsCovidDangerous, Priority, Count, Perceptor, ResultPath):-
    IsCovidDangerous == 0,
    Priority >= 1,

    Count1 is Count + 1,
    check_current_minimum(From, To, Count1),

    append(LeadingPath, [From], CurrentPath),

    get_next_points_with_prioritets(From, To, IsCovidDangerous, CurrentPath, Perceptor, Sorted),
    get(Sorted, [Priority1, From1]),
    backtracking(From1, To, CurrentPath, IsCovidDangerous, Priority1, Count1, Perceptor, ResultPath).

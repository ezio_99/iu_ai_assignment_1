% ======================================================================================
% READING USER INPUT
% ======================================================================================

:- dynamic([
    is_user_input/1,       % will user enter params or not
    world_width_height/2,  % world dimensions specified by user, default [9, 9]
    mask_count/1,          % quantity of masks in board specified by user, default 1
    doctor_count/1,        % quantity of doctors in board specified by user, default 1
    covid_count/1          % quantity of covids in board specified by user, default 2
]).

% read map params from input stream
read_user_input:- \+ is_user_input(1).

read_user_input:-
    is_user_input(1),

    retractall(world_width_height(_, _)),
    retractall(mask_count(_)),
    retractall(doctor_count(_)),
    retractall(covid_count(_)),

    format('World width: '),      read(X),
    format('World height: '),     read(Y),
    format('Masks quantity: '),   read(MaskCount),
    format('Doctors quantity: '), read(DoctorCount),
    format('Covids quantity: '),  read(CovidCount),

    assert(world_width_height(X, Y)),
    assert(mask_count(MaskCount)),
    assert(doctor_count(DoctorCount)),
    assert(covid_count(CovidCount)),

    retractall(is_user_input(_)).

read:- clear, new_map, assert(is_user_input(1)), read_user_input.

% ======================================================================================
% BOARD INITIALIZATION
% ======================================================================================

:- dynamic([
    world_size/1,          % size of the board, world_size([X, Y])
    position/2             % position of A, position(A, [X, Y])
]).

% clear board before initialization
clear:-
    retractall(world_size(_)),
    retractall(position(_, _)).

% initialize board
init_board:-
    clear,
    init_world,
    init_agent,
    init_home,
    init_doctor,
    init_mask,
    init_covid.

% get specified by user map parameters
% if no specified parameter - return default
get_map_params(X, Y, MascCount, DoctorCount, CovidCount):-
    (world_width_height(X, Y)  -> true; X = 9, Y = 9),
    (mask_count(MascCount)     -> true; MascCount   = 1),
    (doctor_count(DoctorCount) -> true; DoctorCount = 1),
    (covid_count(CovidCount)   -> true; CovidCount  = 2).

% initialize world if map is provided by user
init_world:-
    world_dimension(Size),
    assert(world_size(Size)).

% generate world if map is not provided by user
init_world:-
    \+ world_dimension(_),
    get_map_params(X, Y, MascCount, DoctorCount, CovidCount),
    generate_map(X, Y, MascCount, DoctorCount, CovidCount),
    world_dimension(Size),
    assert(world_size(Size)).

init_agent:- start_position(Position), assert(position(agent, Position)).

init_home:- forall(home_position(X, Y), assert(position(home, [X, Y]))).

init_doctor:- forall(doctor_position(X, Y), assert(position(doctor, [X, Y]))).

init_mask:- forall(mask_position(X, Y), assert(position(mask, [X, Y]))).

init_covid:- forall(covid_position(X, Y), assert(position(covid, [X, Y]))).

% ======================================================================================
% SENSORS
% ======================================================================================

% variant 1 covid perception
perceptor1(From, Cell):- adjacent(From, Cell), is_covid(Cell).

% variant 2 covid perception
perceptor2(From, Cell):- adjacent2(From, Cell), is_covid(Cell).
perceptor2(From, Cell):- adjacent(From, Cell), is_covid(Cell).

% is cell contains covid
is_covid(Cell):- position(covid, Z), adjacent(Z, Z1), Z1 == Cell.

% is cell contains mask
is_mask(Cell):- position(mask, Z), Z == Cell.

% is cell contains doctor
is_doctor(Cell):- position(doctor, Z), Z == Cell.

% is cell contains home
is_home(Cell):- position(home, Z), Z == Cell.

% check if given cell contains mask or doctor
is_covid_is_not_dangerous(Cell):- is_mask(Cell).
is_covid_is_not_dangerous(Cell):- is_doctor(Cell).

% ======================================================================================
% HELPERS
% ======================================================================================

% check if given cell in the board
is_valid_pos([X, Y]):- X > -1, Y > -1, world_dimension([P, Q]), X < P, Y < Q.

% return adjacent cells with radius 1
adjacent([X, Y], Z):- Left  is X - 1,                 is_valid_pos([Left,  Y    ]), Z = [Left,  Y    ].
adjacent([X, Y], Z):- Right is X + 1,                 is_valid_pos([Right, Y    ]), Z = [Right, Y    ].
adjacent([X, Y], Z):- Above is Y + 1,                 is_valid_pos([X,     Above]), Z = [X,     Above].
adjacent([X, Y], Z):- Below is Y - 1,                 is_valid_pos([X,     Below]), Z = [X,     Below].
adjacent([X, Y], Z):- Left  is X - 1, Above is Y + 1, is_valid_pos([Left,  Above]), Z = [Left,  Above].
adjacent([X, Y], Z):- Left  is X - 1, Below is Y - 1, is_valid_pos([Left,  Below]), Z = [Left,  Below].
adjacent([X, Y], Z):- Right is X + 1, Above is Y + 1, is_valid_pos([Right, Above]), Z = [Right, Above].
adjacent([X, Y], Z):- Right is X + 1, Below is Y - 1, is_valid_pos([Right, Below]), Z = [Right, Below].

% return adjacent cells with radius 2
adjacent2([X, Y], Z):- Left   is X - 2,                  is_valid_pos([Left,   Y     ]), Z = [Left,   Y     ].
adjacent2([X, Y], Z):- Right  is X + 2,                  is_valid_pos([Right,  Y     ]), Z = [Right,  Y     ].
adjacent2([X, Y], Z):- Above  is Y + 2,                  is_valid_pos([X,      Above ]), Z = [X,      Above ].
adjacent2([X, Y], Z):- Below  is Y - 2,                  is_valid_pos([X,      Below ]), Z = [X,      Below ].
adjacent2([X, Y], Z):- Left   is X - 2, Above  is Y + 2, is_valid_pos([Left,   Above ]), Z = [Left,   Above ].
adjacent2([X, Y], Z):- Left   is X - 2, Below  is Y - 2, is_valid_pos([Left,   Below ]), Z = [Left,   Below ].
adjacent2([X, Y], Z):- Right  is X + 2, Above  is Y + 2, is_valid_pos([Right,  Above ]), Z = [Right,  Above ].
adjacent2([X, Y], Z):- Right  is X + 2, Below  is Y - 2, is_valid_pos([Right,  Below ]), Z = [Right,  Below ].
adjacent2([X, Y], Z):- Left2  is X - 2, Above1 is Y + 1, is_valid_pos([Left2,  Above1]), Z = [Left2,  Above1].
adjacent2([X, Y], Z):- Left2  is X - 2, Below1 is Y - 1, is_valid_pos([Left2,  Below1]), Z = [Left2,  Below1].
adjacent2([X, Y], Z):- Left1  is X - 1, Above2 is Y + 2, is_valid_pos([Left1,  Above2]), Z = [Left1,  Above2].
adjacent2([X, Y], Z):- Left1  is X - 1, Below2 is Y - 2, is_valid_pos([Left1,  Below2]), Z = [Left1,  Below2].
adjacent2([X, Y], Z):- Right2 is X + 2, Above1 is Y + 1, is_valid_pos([Right2, Above1]), Z = [Right2, Above1].
adjacent2([X, Y], Z):- Right2 is X + 2, Below1 is Y - 1, is_valid_pos([Right2, Below1]), Z = [Right2, Below1].
adjacent2([X, Y], Z):- Right1 is X + 1, Above2 is Y + 2, is_valid_pos([Right1, Above2]), Z = [Right1, Above2].
adjacent2([X, Y], Z):- Right1 is X + 1, Below2 is Y - 2, is_valid_pos([Right1, Below2]), Z = [Right1, Below2].

max(A, B, R):- (A > B -> R = A; R = B).

min(A, B, R):- (A > B -> R = B; R = A).

abs(A, R):- A >= 0, R = A.
abs(A, R):- A < 0, R is -A.

% return first 7 elements from list
% helpful to call recursive functions for elements in list
get(L, E):- nth1(1, L, E).
get(L, E):- nth1(2, L, E).
get(L, E):- nth1(3, L, E).
get(L, E):- nth1(4, L, E).
get(L, E):- nth1(5, L, E).
get(L, E):- nth1(6, L, E).
get(L, E):- nth1(7, L, E).

% Chebyshed distance between 2 points
distance([Xa, Ya], [Xb, Yb], D):-
    DX1 is Xa - Xb, abs(DX1, DX),
    DY1 is Ya - Yb, abs(DY1, DY),
    max(DX, DY, D).

% ======================================================================================

% get shortest path in list with paths

get_shortest_path([], Result):- Result = [].

get_shortest_path([A], Result):- Result = A.

get_shortest_path([A, B | T], Result):-
    length(A, LenA),
    length(B, LenB),
    (
        LenA > LenB ->

        append([B], T, T1), get_shortest_path(T1, Result)
        ;
        append([A], T, T2), get_shortest_path(T2, Result)
    ).

% ======================================================================================
% DRAWING THE MAP
% ======================================================================================

% draw cell depending on what cell contains
print_cell(Cell):- position(covid,  CovidPosition ), Cell == CovidPosition,  format('C '), !.
print_cell(Cell):- position(agent,  AgentPosition ), Cell == AgentPosition,  format('A '), !.
print_cell(Cell):- position(mask,   MaskPosition  ), Cell == MaskPosition,   format('M '), !.
print_cell(Cell):- position(doctor, DoctorPosition), Cell == DoctorPosition, format('D '), !.
print_cell(Cell):- position(home,   HomePosition  ), Cell == HomePosition,   format('H '), !.
print_cell(Cell):- path_to_print(Path), member(Cell, Path), format('* '), !.
print_cell(Cell):- is_covid(Cell), format('! '), !.
print_cell(_):- format('. '), !.

% print cells with coordinates [ColumnStart .. ColumnEnd, Row]
print_row(_, ColumnStart, ColumnEnd):- ColumnStart > ColumnEnd.
print_row(Row, ColumnStart, ColumnEnd):-
    ColumnStart =< ColumnEnd,
    print_cell([ColumnStart, Row]),
    ColumnStart1 is ColumnStart + 1,
    print_row(Row, ColumnStart1, ColumnEnd).

% print cells with coordinates [ColumnStart .. ColumnEnd, Start .. End]
print_rows(Start, End, _, _):- Start < End.
print_rows(Start, End, ColumnStart, ColumnEnd):-
    Start >= End,
    format('| '),
    print_row(Start, ColumnStart, ColumnEnd),
    format('|~n'),
    Start1 is Start - 1,
    print_rows(Start1, End, ColumnStart, ColumnEnd).

% print Arg N times
repeat_format(_, N):- N =< 0.
repeat_format(Arg, N):-
    N > 0,
    format(Arg),
    N1 is N - 1,
    repeat_format(Arg, N1).

% print board
print_board:-
    world_size([X, Y]),
    N is X + X + 3,
    repeat_format('-', N), nl,
    X1 is X - 1,
    Y1 is Y - 1,
    print_rows(Y1, 0, 0, X1),
    repeat_format('-', N), nl.

:- dynamic([
    path_to_print/1   % used only in print_path
]).

% print board with given Path
print_path(Path):-
    retractall(path_to_print(_)),
    assert(path_to_print(Path)),
    print_board,
    retractall(path_to_print(_)).

% ======================================================================================
% PRINTING THE RESULT
% ======================================================================================

% print time
print_time(Time):-
    format('Time: ~w seconds~n', [Time]).

% print result if path is not exists
print_result([], Time):-
    format('Lose~nNo possible paths to home~n'),
    print_time(Time),
    format('~n').

% print result if path is exists
print_result(Path, Time):-
    length(Path, Length), Length1 is Length - 1,
    format('Win~nShortest path to home: ~w~nSteps: ~w~n', [Path, Length1]),
    print_time(Time),
    format('~nShortest path:~n'),
    print_path(Path),
    format('~n').

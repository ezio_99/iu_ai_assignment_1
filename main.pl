:- use_module(library(lists)).

:- ['lib/common.pl'].
:- ['lib/backtracking.pl'].
:- ['lib/a_star.pl'].
:- ['lib/random.pl'].
% :- ['maps/1.pl'].

% World params
world_width_height(9, 9).
mask_count(1).
doctor_count(1).
covid_count(2).

% Use user input or not
is_user_input(1).

% main flow for all algorithms
% requires search algorithm with contract
% search(StartPosition, DestinationPosition, ShortestPath)
main(SearchAlgorithm):-
    read_user_input,

    init_board,

    format('Algorithm: ~w~n~n', [SearchAlgorithm]),
    format('Initial map: ~n'),
    print_board,
    format('~n'),

    position(agent, AgentPosition),
    position(home, HomePosition),

    get_time(StartTime),
    call(SearchAlgorithm, AgentPosition, HomePosition, ShortestPath),
    get_time(EndTime),

    Time is EndTime - StartTime,
    print_result(ShortestPath, Time).

test_backtrack1:- main(backtracking_1).
test_backtrack2:- main(backtracking_2).
test_a_star1:- main(a_star_1).
test_a_star2:- main(a_star_2).

b1:- test_backtrack1.
b2:- test_backtrack2.
a1:- test_a_star1.
a2:- test_a_star2.
